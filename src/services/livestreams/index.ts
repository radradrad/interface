interface AddTrackBody {
  title: string,
  url: string,
}

interface ApiData {
  [key: string]: any;
}

class LivestreamsService {
  baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  async getLivestream(id: string): Promise<ApiData> {
    const endpoint = "/livestreams"
    const url = `${this.baseUrl}${endpoint}/${id}`;
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer YOUR_TOKEN' // if needed
      },
    });

    if (!response.ok) {
      throw new Error(`GET request failed: ${response.status}`);
    }

    const data = await response.json();
    return data;
  }

  async addTrack(body: AddTrackBody): Promise<string> {
    const endpoint = "/livestreams"
    const url = `${this.baseUrl}${endpoint}`;
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Authorization': 'Bearer YOUR_TOKEN' // if needed
      },
      body: JSON.stringify(body),
    });

    if (!response.ok) {
      throw new Error(`POST request failed: ${response.status}`);
    }

    const data = await response.json();
    return data;
  }

  async poll(id: string, interval = 3000): Promise<ApiData> {
    // eslint-disable-next-line no-async-promise-executor
    return new Promise(async (resolve, reject) => {
      try {
        const data = await this.getLivestream(id);
        if (data.status === 'crated') {
          resolve(data);
          return;  // Return here since status is 'done'
        }
      } catch (error) {
        reject(error);
        return;  // Return here in case of an error
      }
      const intervalId = setInterval(async () => {
        try {
          const data = await this.getLivestream(id);
          if (data.status === 'crated') {
            clearInterval(intervalId); // stop polling when status is 'done'
            resolve(data);
          }
        } catch (error) {
          clearInterval(intervalId); // stop polling in case of error
          reject(error);
        }
      }, interval);
    });
  }
}

export default LivestreamsService