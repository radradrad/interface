import { h, createContext } from 'preact';
import { Route, Router } from 'preact-router';

import Header from './header';

// Code-splitting is automated for `routes` directory
import Session from '../routes/session'
import Stream from '../routes/stream';
import About from '../routes/about'
import LivestreamsService from '../services/livestreams';

export const ServiceContext = createContext<LivestreamsService | undefined>(undefined);

export const ServiceProvider: React.FC<{ livestreams: LivestreamsService }> = ({ children, livestreams }) => {
	return (
		<ServiceContext.Provider value={livestreams}>
			{children}
		</ServiceContext.Provider>
	);
};

const App = () => {
	const livestreams = new LivestreamsService("https://api.rad.zantwi.ch")
	return (
		<div id="app">
			<Header />
			<main>
				<ServiceProvider livestreams={livestreams}>
					<Router>
						<Route path="/" component={Session} />
						<Route path="/stream/:session" component={Stream} />
						<Route path="/about" component={About} user="me" />
					</Router>
				</ServiceProvider>
			</main>
		</div>
	)
};

export default App;
