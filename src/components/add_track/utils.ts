export function cleanUrl(string: string) {
  let url: URL;

  try {
    url = new URL(string);
  } catch (_) {
    return "";
  }

  if (url.protocol === "http:" || url.protocol === "https:") {
    return url.toString().replace(url.search, "")
  }

  return ""
}