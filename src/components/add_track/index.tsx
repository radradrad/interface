import { h } from 'preact';
import { useContext, useState } from 'preact/hooks';
import { ServiceContext } from '../app';
import { route } from 'preact-router';
import style from './style.css';
import { cleanUrl } from './utils';

type StatusType = "error" | "info"

interface Status {
  type: StatusType,
  message: string
}

const AddTrack = () => {
  const [status, _setStatus] = useState<Status>({ type: "info", message: "" });
  const livestreams = useContext(ServiceContext)

  const setStatus = (type: StatusType, message?: string) => _setStatus({ type, message: message || "" })

  const handleKeyDown = async (e) => {
    const url = cleanUrl(e.currentTarget.value)
    if (e.key === 'Enter') {
      if (!url) {
        return setStatus('error', 'please set a valid url')
      }
      setStatus('info')

      const track = { title: url.split("/")[url.split("/").length - 1], url }
      try {
        const id = await livestreams.addTrack(track)
        setStatus("info", "loading........")
        const res = await livestreams.poll(id)
        route(`/stream/${res.title}`)
      } catch (e) {
        setStatus('error', 'an error occurred when creating the livestream')
      }
    }
  }

  const getEmoji = (status: StatusType) => {
    switch (status) {
      case "error":
        return "🤕";
      case "info":
        return "🫧"
    }
  }

  return (
    <div class={style.container}>
      <span class={style.title}>
        add track:
        <br />
      </span>
      <input onKeyPress={handleKeyDown} class={style.input} />
      <br />
      {status.message && `${getEmoji(status.type)} ${status.message}`}
    </div>
  )
};

export default AddTrack;
