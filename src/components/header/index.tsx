import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style.css';

const Header = () => (
	<header class={style.header}>
		<a href="/" class={style.logo}>
			<h1>rad</h1>
		</a>
		<nav>
			<Link activeClassName={style.active} href="/">
				stream
			</Link>
			<Link activeClassName={style.active} href="/about">
				about
			</Link>
		</nav>
	</header>
);

export default Header;
