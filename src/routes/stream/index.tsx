import { h } from 'preact';
import style from './style.css';
import Hls from 'hls.js'
import { useEffect, useRef } from 'preact/hooks';

const Stream = ({ session }) => {
	const ref = useRef<HTMLVideoElement>()
	useEffect(() => {
		const domain = "https://transcoder.rad.zantwi.ch"
		const value = session
		const video = ref.current
		if (Hls.isSupported()) {
			const hls = new Hls({
				debug: true,
			});
			hls.loadSource(`${domain}/stream/${value}.m3u8`);
			if (video) {
				hls.attachMedia(video);
				hls.on(Hls.Events.MANIFEST_PARSED, () => {
					video.muted = true;
					video.play();
				});
			}
		}
	}, [])
	return (
		<div class={style.session}>
			{/* {ref.current && */}
			<div>
				listening to ✨{session}✨!
				<br />
				(remember to un-mute) <br />
				<video ref={ref} controls class={style.video} />
			</div>
			{/* } */}
		</div>
	);
};

export default Stream;
